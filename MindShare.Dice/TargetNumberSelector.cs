﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="TargetNumberSelector.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System.Collections.Generic;

using MindShare.Dice.Interfaces;

namespace MindShare.Dice
{
   public class TargetNumberSelector : IDiceSelector
   {
      #region Constructors And Finalizers

      /// <summary>
      ///    Initializes a new instance of the <see cref="TargetNumberSelector" /> class.
      /// </summary>
      /// <param name="targetNumber">The target number to compare against</param>
      public TargetNumberSelector(int targetNumber)
      {
         TargetNumber = targetNumber;
      }

      #endregion

      public int TargetNumber { get; }

      #region IDiceSelector Members

      /// <summary>
      /// Evaluates dice based on criteria internal to the particular <see cref="IDiceSelector" /> implementation and returns the selected dice.
      /// </summary>
      /// <param name="dice">The dice to evaluate for selection.</param>
      /// <returns>Returns a <see cref="IEnumerable{T}" /> containing any selected dice.</returns>
      public IList<IDie> Select(IEnumerable<IDie> dice)
      {
         var results = new List<IDie>();
         foreach (var die in dice)
         {
            var selected = EvaluateDieForSelection(die);
            if (selected != null)
               results.Add(selected);
         }

         return results;
      }

      #endregion

      /// <summary>
      /// Evaluates the die for selection.
      /// </summary>
      /// <param name="die">The die to evaluate.</param>
      /// <returns>Returns the supplied <see cref="IDie"/> if it is selected or <see landword="null"/> otherwise.</returns>
      protected internal IDie EvaluateDieForSelection(IDie die)
      {
         var numericDie = die as INumericDie;

         if (numericDie?.GetFaceUpValue() >= TargetNumber)
            return numericDie;

         return null;
      }
   }
}