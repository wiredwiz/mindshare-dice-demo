﻿#region Dansko LLC License

// <copyright file="NumericDie.cs" company="Dansko LLC">
// Copyright 2016 Dansko LLC
// </copyright>
// This code is property of Dansko LLC and is not licensed for use by any other parties.

#endregion

using System;
using System.Collections.Generic;

using MindShare.Dice.Interfaces;

namespace MindShare.Dice
{
   public class NumericDie : INumericDie
   {
      #region Constructors And Finalizers

      public NumericDie(IList<int> sides)
      {
         Sides = sides;
      }

      #endregion

      #region INumericDie Members

      /// <summary>
      ///    Gets or sets the current face up side.
      /// </summary>
      /// <value>A number that identifies the current face up side.</value>
      /// <remarks>
      ///    In the case of non-numeric dice this value could be the integer equivalent of an enumeration value that
      ///    identifies a side.
      /// </remarks>
      public virtual int CurrentFaceUpSide { get; set; }

      /// <summary>
      ///    Gets the value of the current face up side on the die.
      /// </summary>
      /// <returns>The numeric value.</returns>
      /// <exception cref="ArgumentOutOfRangeException">
      ///    CurrentFaceUpSide is not a valid index in the list of numeric sides.
      /// </exception>
      public virtual int GetFaceUpValue()
      {
         return Sides[CurrentFaceUpSide];
      }

      /// <summary>
      ///    Gets the list of numbers on the sides of the die.
      /// </summary>
      /// <value>A <see cref="List{Int32}" /> of the numeric values.</value>
      public virtual IList<int> Sides { get; }

      public virtual IDiceRollResult Roll()
      {
         CurrentFaceUpSide = RandomNumberGenerator.Current.Next(Sides.Count);
         return null;
      }

        #endregion

        public override string ToString()
        {
            return GetFaceUpValue().ToString();
        }
    }
}