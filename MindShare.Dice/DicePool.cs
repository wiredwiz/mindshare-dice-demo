﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="DicePool.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Collections.Generic;
using System.Linq;

using MindShare.Dice.Interfaces;

using Org.Edgerunner.FluentGuard;

namespace MindShare.Dice
{
   public class DicePool : IDicePool
   {
      #region Constructors And Finalizers

      /// <summary>
      ///    Initializes a new instance of the <see cref="DicePool" /> class.
      /// </summary>
      /// <param name="dice">The dice to add to the pool.</param>
      public DicePool(IEnumerable<IDie> dice)
      {
         Validate.That(nameof(dice), dice).IsNotNull().OtherwiseThrowException();

         Dice = dice as IList<IDie> ?? dice.ToList();
      }

      /// <summary>
      ///    Initializes a new instance of the <see cref="DicePool" /> class.
      /// </summary>
      /// <param name="size">The default size of the internal list.</param>
      /// <exception cref="ArgumentOutOfRangeException">
      ///   <paramref name="size" /> is less than 0.
      /// </exception>
      public DicePool(int size)
      {
         Dice = new List<IDie>(size);
      }

      /// <summary>
      ///    Initializes a new instance of the <see cref="DicePool" /> class.
      /// </summary>
      public DicePool()
      {
         Dice = new List<IDie>();
      }

      #endregion

      #region IDicePool Members

      /// <summary>
      /// Adds the specified die.
      /// </summary>
      /// <param name="die">The die.</param>
      /// <returns>The added <see cref="IDie" />.</returns>
      public IDie Add(IDie die)
      {
         // ReSharper disable once ExceptionNotDocumentedOptional
         Dice.Add(die);
         return die;
      }

      /// <summary>
      /// Clears the <see cref="IDicePool" /> dice.
      /// </summary>
      public void Clear()
      {
         // ReSharper disable once ExceptionNotDocumentedOptional
         Dice.Clear();
      }

      /// <summary>
      /// Gets an enumeration of the dice within the pool.
      /// </summary>
      /// <value>The dice enumeration.</value>
      /// <seealso cref="IEnumerable{T}" />
      public IList<IDie> Dice { get; }

      /// <summary>
      /// Rolls this instance.
      /// </summary>
      /// <returns>A new <see cref="IDiceRollResult" />.</returns>
      public IDiceRollResult Roll()
      {
         foreach (var die in Dice)
            die.Roll();

         var result = new DiceRollResult(this);
         return result;
      }

      #endregion
   }
}