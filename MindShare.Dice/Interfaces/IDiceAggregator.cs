﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="IDiceAggregator.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Collections.Generic;

namespace MindShare.Dice.Interfaces
{
   /// <summary>
   ///    Interface that defines an object for calculating aggregate die values of some type
   /// </summary>
   public interface IDiceAggregator
   {
      /// <summary>
      /// Gets the aggregated value from a list of dice.
      /// </summary>
      /// <param name="dice">The dice to be aggregated into a value.</param>
      /// <returns>The aggregated value.</returns>
      int GetValue(IEnumerable<IDie> dice);
   }
}