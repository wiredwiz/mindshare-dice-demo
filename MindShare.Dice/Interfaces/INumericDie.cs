﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="INumericDie.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Collections.Generic;

namespace MindShare.Dice.Interfaces
{
   /// <summary>
   ///    Interface INumericDie
   /// </summary>
   /// <seealso cref="MindShare.Dice.Interfaces.IDie" />
   public interface INumericDie : IDie
   {
      /// <summary>
      /// Gets the list of numbers on the sides of the die.
      /// </summary>
      /// <value>A <see cref="List{Int32}"/> of the numeric values.</value>
      IList<int> Sides { get; }

      /// <summary>
      /// Gets the value of the current face up side on the die.
      /// </summary>
      /// <returns>The numeric value.</returns>
      /// <exception cref="ArgumentOutOfRangeException">
      ///    CurrentFaceUpSide is not a valid index in the list of numeric sides.
      /// </exception>
      int GetFaceUpValue();
   }
}