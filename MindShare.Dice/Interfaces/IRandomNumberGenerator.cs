﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="IRandomNumberGenerator.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;

namespace MindShare.Dice.Interfaces
{
   public interface IRandomNumberGenerator
   {
      /// <summary>
      ///    Generates a random number greater than or equal to 0 and less than the upper bound.
      /// </summary>
      /// <param name="high">The exclusive upper bound.</param>
      /// <returns>A random integer.</returns>
      int Next(int high);

      /// <summary>
      ///    Generates a random number greater than or equal to the lower bound and less than the upper bound.
      /// </summary>
      /// <param name="low">The inclusive lower bound.</param>
      /// <param name="high">The exclusive upper bound.</param>
      /// <returns>A random integer.</returns>
      int Next(int low, int high);
   }
}