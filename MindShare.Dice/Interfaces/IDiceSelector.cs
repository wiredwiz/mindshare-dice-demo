﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="IDiceSelector.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Collections.Generic;

namespace MindShare.Dice.Interfaces
{
   /// <summary>
   ///    Interface that specifies a dice selector
   /// </summary>
   public interface IDiceSelector
   {
      /// <summary>
      /// Evaluates dice based on criteria internal to the particular <see cref="IDiceSelector"/> implementation and returns the selected dice.
      /// </summary>
      /// <param name="dice">The dice to evaluate for selection.</param>
      /// <returns>Returns a <see cref="IList{T}"/> containing any selected dice.</returns>
      IList<IDie> Select(IEnumerable<IDie> dice);
   }
}