﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="IDicePool.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System.Collections.Generic;

namespace MindShare.Dice.Interfaces
{
   public interface IDicePool : IRollable
   {
      /// <summary>
      /// Gets an enumeration of the dice within the pool.
      /// </summary>
      /// <value>The dice enumeration.</value>
      /// <seealso cref="IEnumerable{T}" />
      IList<IDie> Dice { get; }

      /// <summary>
      ///    Adds the specified die.
      /// </summary>
      /// <param name="die">The die.</param>
      /// <returns>The added <see cref="IDie" />.</returns>
      IDie Add(IDie die);

      /// <summary>
      ///    Clears the <see cref="IDicePool" /> dice.
      /// </summary>
      void Clear();
   }
}