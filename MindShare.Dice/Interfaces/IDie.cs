﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="IDie.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

namespace MindShare.Dice.Interfaces
{
   public interface IDie : IRollable
   {
      /// <summary>
      /// Gets or sets the current face up side.
      /// </summary>
      /// <value>A number that identifies the current face up side.</value>
      /// <remarks>In the case of non-numeric dice this value could be the integer equivalent of an enumeration value that identifies a side.</remarks>
      int CurrentFaceUpSide { get; set; }
   }
}