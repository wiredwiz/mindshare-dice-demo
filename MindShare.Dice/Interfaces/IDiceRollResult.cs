#region Apache License 2.0

// <copyright company="Edgerunner.org" file="IDiceRollResult.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System.Collections.Generic;

namespace MindShare.Dice.Interfaces
{
   public interface IDiceRollResult
   {
      /// <summary>
      ///    Gets or sets the dice pool.
      /// </summary>
      /// <value>The dice pool.</value>
      IDicePool Pool { get; set; }

      /// <summary>
      ///    Selects dice from the result using the diceSelector.
      /// </summary>
      /// <param name="diceSelector">The diceSelector to use.</param>
      /// <returns>A new <see cref="IEnumerable{IDie}" />.</returns>
      IList<IDie> Select(IDiceSelector diceSelector);

      /// <summary>
      ///    Aggregates the dice from the dice roll into a single value using the specified aggregator.
      /// </summary>
      /// <param name="aggregator">The aggregator to use.</param>
      /// <returns>The aggregated value of the rolled dice.</returns>
      int Aggregate(IDiceAggregator aggregator);
   }
}