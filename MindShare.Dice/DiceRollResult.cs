#region Apache License 2.0

// <copyright company="Edgerunner.org" file="DiceRollResult.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System.Collections.Generic;

using MindShare.Dice.Interfaces;

namespace MindShare.Dice
{
   public class DiceRollResult : IDiceRollResult
   {
      #region Constructors And Finalizers

      /// <summary>
      ///    Initializes a new instance of the <see cref="DiceRollResult" /> class.
      /// </summary>
      /// <param name="pool">The pool within the result</param>
      public DiceRollResult(IDicePool pool)
      {
         Pool = pool;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DiceRollResult"/> class.
      /// </summary>
      /// <param name="dice">The dice.</param>
      public DiceRollResult(IEnumerable<IDie> dice)
      {
         Pool = new DicePool(dice);
      }

      #endregion

      #region IDiceRollResult Members

      public IDicePool Pool { get; set; }

      /// <summary>
      /// Selects dice from the result using the diceSelector.
      /// </summary>
      /// <param name="diceSelector">The diceSelector to use.</param>
      /// <returns>A new <see cref="IEnumerable{IDie}" />.</returns>
      public IList<IDie> Select(IDiceSelector diceSelector)
      {
         return diceSelector.Select(Pool.Dice);
      }

      /// <summary>
      /// Aggregates the dice from the dice roll into a single value using the specified aggregator.
      /// </summary>
      /// <param name="aggregator">The aggregator to use.</param>
      /// <returns>The aggregated value of the rolled dice.</returns>
      public int Aggregate(IDiceAggregator aggregator)
      {
         return aggregator.GetValue(Pool.Dice);
      }

      #endregion
   }
}