﻿#region Dansko LLC License

// <copyright file="CryptographicRandomNumberGenerator.cs" company="Dansko LLC">
// Copyright 2016 Dansko LLC
// </copyright>
// This code is property of Dansko LLC and is not licensed for use by any other parties.

#endregion

using System;

using MindShare.Dice.Interfaces;

using Org.Edgerunner.FluentGuard;

namespace MindShare.Dice
{
   public class CryptographicRandomNumberGenerator : IRandomNumberGenerator
   {
      internal readonly byte[] RandomBytes;

      private readonly System.Security.Cryptography.RNGCryptoServiceProvider _Generator;      

      private int _Offset;

      #region Constructors And Finalizers

      /// <summary>
      /// Initializes a new instance of the <see cref="CryptographicRandomNumberGenerator"/> class.
      /// </summary>
      public CryptographicRandomNumberGenerator()
         : this(1000)
      {
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="CryptographicRandomNumberGenerator"/> class.
      /// </summary>
      /// <param name="numberPoolSize">Size of the number pool.</param>
      public CryptographicRandomNumberGenerator(int numberPoolSize)
      {
         _Generator = new System.Security.Cryptography.RNGCryptoServiceProvider();
         RandomBytes = new byte[numberPoolSize * 4];
         GenerateRandomness();
      }

      #endregion

      #region IRandomNumberGenerator Members

      /// <summary>
      ///    Generates a random number greater than or equal to 0 and less than the upper bound.
      /// </summary>
      /// <param name="high">The exclusive upper bound.</param>
      /// <returns>A random integer.</returns>
      public int Next(int high)
      {
         Validate.That(nameof(high), high).IsNotNegative().OtherwiseThrowException();

         return Next(0, high);
      }

      /// <summary>
      ///    Generates a random number greater than or equal to the lower bound and less than the upper bound.
      /// </summary>
      /// <param name="low">The inclusive lower bound.</param>
      /// <param name="high">The exclusive upper bound.</param>
      /// <returns>A random integer.</returns>
      public int Next(int low, int high)
      {
         Validate.That(nameof(low), low).IsNotNegative().OtherwiseThrowException();
         Validate.That(nameof(high), high).IsGreaterThan(low).OtherwiseThrowException();

         // If our offset has moved past our buffer length then we need to generate more randomness and reset the offset
         // We use Length - 3 because each 32 bit integer requires 4 bytes.
         if (_Offset > RandomBytes.Length - 3)
            GenerateRandomness();

         // Use the next 4 bytes within our buffer of randomness to generate a random integer and then shift the buffer offset forward
         var number = Math.Abs(BitConverter.ToInt32(RandomBytes, _Offset));
         _Offset += 4;
         return ConvertRandomIntegerToRestrictedRange(number, low, high);
      }

      #endregion

      private int ConvertRandomIntegerToRestrictedRange(int number, int lowerBound, int upperBound)
      {
         // First find our value range restriction
         var difference = upperBound - lowerBound;
         // Perform a modulus on our random number to convert it to a number within our desired difference range
         var remainder = number % difference;
         // Return the result of the modulus, adding our lowerBound so that the resulting number now falls within our lower and upper bound
         return lowerBound + remainder;
      }

      internal void GenerateRandomness()
      {
         _Generator.GetBytes(RandomBytes);
         _Offset = 0;
      }
   }
}