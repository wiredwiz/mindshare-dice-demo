﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="DiceRoller.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using MindShare.Dice.Interfaces;

namespace MindShare.Dice
{
   public class DiceRoller : IDiceRoller
   {
      #region Constructors And Finalizers

      /// <summary>
      ///    Initializes a new instance of the <see cref="DiceRoller" /> class.
      /// </summary>
      /// <param name="factory">The factory to use.</param>
      public DiceRoller(IDiceFactory factory)
      {
         Factory = factory;
         // Random dice rolls can easily be converted to to cryptographically strong numbers by un-commenting the next line
         RandomNumberGenerator.Current = new CryptographicRandomNumberGenerator();
      }

      #endregion

      protected IDiceFactory Factory { get; }

      #region IDiceRoller Members

      public IDiceRollResult Roll(string dicePattern)
      {
         var pool = Factory.Create(dicePattern);
         return pool.Roll();
      }

      #endregion
   }
}