﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="RandomNumberGenerator.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using MindShare.Dice.Interfaces;

using Org.Edgerunner.FluentGuard;

namespace MindShare.Dice
{
   using System;

   public class RandomNumberGenerator : IRandomNumberGenerator
   {
      #region Constructors And Finalizers

      /// <summary>
      ///    Initializes a new instance of the <see cref="RandomNumberGenerator" /> class.
      /// </summary>
      public RandomNumberGenerator()
      {
         Generator = new Random();
      }

      #endregion

      public static IRandomNumberGenerator Current { get; set; }

      private Random Generator { get; }

      #region IRandomNumberGenerator Members

      /// <summary>
      /// Generates a random number greater than or equal to 0 and less than the upper bound.
      /// </summary>
      /// <param name="high">The exclusive upper bound.</param>
      /// <returns>A random integer.</returns>
      /// <exception cref="ArgumentOutOfRangeException">
      ///   <paramref name="high" /> is less than zero. 
      /// </exception>
      public int Next(int high)
      {
         Validate.That(nameof(high), high).IsNotNegative().OtherwiseThrowException();

         return Generator.Next(high);
      }

      /// <summary>
      /// Generates a random number greater than or equal to the lower bound and less than the upper bound.
      /// </summary>
      /// <param name="low">The inclusive lower bound.</param>
      /// <param name="high">The exclusive upper bound.</param>
      /// <returns>A random integer.</returns>
      /// <exception cref="ArgumentOutOfRangeException">
      ///   <paramref name="low" /> is greater than <paramref name="high" />. 
      /// </exception>
      public int Next(int low, int high)
      {
         Validate.That(nameof(low), low).IsNotNegative().OtherwiseThrowException();
         Validate.That(nameof(high), high).IsGreaterThan(low).OtherwiseThrowException();

         return Generator.Next(low, high);
      }

      #endregion
   }
}