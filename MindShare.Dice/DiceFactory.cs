﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="DiceFactory.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System.Collections.Generic;

using MindShare.Dice.Exceptions;
using MindShare.Dice.Interfaces;

namespace MindShare.Dice
{
   public class DiceFactory : IDiceFactory
   {
      #region IDiceFactory Members

      /// <summary>
      ///    Creates a new <see cref="IDicePool" /> from the specified dice pattern.
      /// </summary>
      /// <param name="dicePattern">The dice pattern.</param>
      /// <returns>A new <see cref="IDicePool" />.</returns>
      /// <exception cref="InvalidDieExpressionException">The dice pattern was invalid.</exception>
      public IDicePool Create(string dicePattern)
      {
         var index = dicePattern.IndexOf('d');
         if (index == -1)
            index = dicePattern.IndexOf('D');
         if (index == -1)
            throw new InvalidDieExpressionException($"\"{dicePattern}\" is not a valid dice expression");

         var numberOfDiceText = dicePattern.Substring(0, index);
         var dieTypeText = dicePattern.Substring(index + 1);
         int numberOfDice;
         int dieType;
         if (!int.TryParse(numberOfDiceText, out numberOfDice))
            throw new InvalidDieExpressionException($"\"{dicePattern}\" is not a valid dice expression");
         if (!int.TryParse(dieTypeText, out dieType))
            throw new InvalidDieExpressionException($"\"{dicePattern}\" is not a valid dice expression");

         var pool = new DicePool(numberOfDice);
         for (int i = 1; i <= numberOfDice; i++)
         {
            var sides = new List<int>(dieType);
            for (int j = 1; j < dieType; j++)
               sides.Add(j);
            var die = new NumericDie(sides);
            pool.Add(die);
         }

         return pool;
      }

      #endregion
   }
}