﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindShare.Dice;

namespace DiceRollerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller(new DiceFactory());
            Console.WriteLine("Please enter dice roll in format of #d#");
            var diceRequest = Console.ReadLine();
            Console.WriteLine("Please enter the target number for the dice");
            var targetRequest = Console.ReadLine();
            var results = diceRoller.Roll(diceRequest);
            Console.WriteLine($"Your result is :{string.Join(", ", results.Pool.Dice)}");
            TargetNumberSelector targetSelector = new TargetNumberSelector(int.Parse(targetRequest));
            Console.WriteLine($"The successful rolls were :{string.Join(", ", results.Select(targetSelector))}"); 
            Console.ReadLine();
        }
    }
}
