﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="CryptographicRandomNumberGeneratorTests.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Collections.Generic;

using FluentAssertions;

using MindShare.Dice.Interfaces;

using Xbehave;

namespace MindShare.Dice.Tests
{
   public class CryptographicRandomNumberGeneratorTests
   {
      [Scenario]
      [Example(1, 15)]
      [Example(3, 4)]
      public void RandomNumberIsGreaterThanOrEqualToLowAndLessThanHigh(
         int low,
         int high,
         int value,
         IRandomNumberGenerator generator)
      {
         "Given a new cryptographic Random Number Generator"
            .x(() => generator = new CryptographicRandomNumberGenerator());

         "And Generating a random number from low to max"
            .x(() => value = generator.Next(low, high));

         "The resulting random number is greater than or equal the low and less than the max"
            .x(() => value.Should().BeGreaterOrEqualTo(low).And.BeLessThan(high));
      }

      [Scenario]
      [Example(1)]
      [Example(15)]
      [Example(3)]
      public void RandomNumberIsLessThanHigh(int high, int value, IRandomNumberGenerator generator)
      {
         "Given a new cryptographic Random Number Generator"
            .x(() => generator = new CryptographicRandomNumberGenerator());

         "And Generating a random number from low to max"
            .x(() => value = generator.Next(high));

         "The resulting random number is greater than or equal the low and less than the max"
            .x(() => value.Should().BeLessThan(high));
      }

      /// <summary>
      /// Tests that exceeding the internal buffer of randomness generates new bytes.
      /// </summary>
      /// <param name="randomNumberSize">The size of the random number byte pool.</param>
      /// <param name="generator">The cryptographic random number generator.</param>
      /// <param name="firstPool">A copy of the first pool.</param>
      [Scenario]
      [Example(4)]
      [Example(200)]
      [Example(2000)]
      public void ExceedingBufferGeneratesNewNumbers(int randomNumberSize, CryptographicRandomNumberGenerator generator, byte[] firstPool)
      {
         "Given a new cryptographic Random Number Generator with a specified pool size of numbers"
            .x(() => generator = new CryptographicRandomNumberGenerator(randomNumberSize));

         "Then copying the existing pool"
            .x(() => generator.RandomBytes.CopyTo(firstPool = new byte[randomNumberSize * 4], 0));

         "And generating a new pool of equal size"
            .x(() => generator.GenerateRandomness());

         "New pool must be different than the original pool"
            .x(() => firstPool.Should().NotBeEquivalentTo(generator.RandomBytes));
      }
   }
}