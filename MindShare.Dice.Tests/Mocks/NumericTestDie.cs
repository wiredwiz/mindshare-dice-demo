﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="NumericTestDie.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Collections.Generic;

using MindShare.Dice.Interfaces;

namespace MindShare.Dice.Tests.Mocks
{
   public class NumericTestDie : NumericDie
   {
      #region Constructors And Finalizers

      /// <summary>
      ///    Initializes a new instance of the <see cref="NumericTestDie" /> class.
      /// </summary>
      /// <param name="sides">The sides.</param>
      /// <param name="results">The results.</param>
      public NumericTestDie(IList<int> sides, IList<int> results)
         : base(sides)
      {
         Results = results;
         CurrentIteration = -1;
         CurrentFaceUpSide = 0;
      }

      #endregion

      public int CurrentIteration { get; set; }

      public IList<int> Results { get; }

      #region INumericDie Members

      /// <summary>
      ///    Rolls this instance.
      /// </summary>
      /// <returns>A new <see cref="T:MindShare.Dice.Interfaces.IDiceRollResult" />.</returns>
      public override IDiceRollResult Roll()
      {
         CurrentIteration++;
         CurrentIteration = CurrentIteration % (Results.Count + 1);

         // We subtract 1 so that the numeric side gets converted to a 0 based index.
         CurrentFaceUpSide = Results[CurrentIteration] - 1;
         return null;
      }

      #endregion
   }
}