﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="NumericDiceTestFactory.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System.Collections.Generic;

using MindShare.Dice.Interfaces;

namespace MindShare.Dice.Tests.Mocks
{
   public class NumericDiceTestFactory : IDiceFactory
   {
      #region Constructors And Finalizers

      /// <summary>
      ///    Initializes a new instance of the <see cref="NumericDiceTestFactory" /> class.
      /// </summary>
      /// <param name="dice">Test dice to use.</param>
      public NumericDiceTestFactory(List<IDie> dice)
      {
         Dice = dice;
      }

      #endregion

      public List<IDie> Dice { get; set; }

      #region IDiceFactory Members

      public IDicePool Create(string dicePattern)
      {
         return new DicePool(Dice);
      }

      #endregion
   }
}