﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="RandomNumberGeneratorTests.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;

using FluentAssertions;

using MindShare.Dice.Interfaces;

using Xbehave;

namespace MindShare.Dice.Tests
{
   public class RandomNumberGeneratorTests
   {
      [Scenario]
      [Example(1, 15)]
      [Example(3, 4)]
      public void RandomNumberIsGreaterThanOrEqualToLowAndLessThanHigh(
         int low,
         int high,
         int value,
         IRandomNumberGenerator generator)
      {
         "Given a new Random Number Generator"
            .x(() => generator = new RandomNumberGenerator());

         "And Generating a random number from low to max"
            .x(() => value = generator.Next(low, high));

         "The resulting random number is greater than or equal the low and less than max"
            .x(() => value.Should().BeGreaterOrEqualTo(low).And.BeLessThan(high));
      }

      [Scenario]
      [Example(1)]
      [Example(15)]
      [Example(3)]
      public void RandomNumberIsLessThanHigh(int high, int value, IRandomNumberGenerator generator)
      {
         "Given a new Random Number Generator"
            .x(() => generator = new RandomNumberGenerator());

         "And Generating a random number from low to max"
            .x(() => value = generator.Next(high));

         "The resulting random number is greater than or equal the low and less than max"
            .x(() => value.Should().BeLessThan(high));
      }
   }
}