﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="NumericDiceSelectorTests.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System;
using System.Collections.Generic;

using FluentAssertions;

using MindShare.Dice.Interfaces;
using MindShare.Dice.Tests.Generators;

using Xbehave;

namespace MindShare.Dice.Tests
{
   public class NumericDiceSelectorTests
   {
      /// <summary>
      ///    Tests to a <see cref="TargetNumberSelector" /> to insure that dice are not selected when appropriate.
      /// </summary>
      /// <param name="numberOfSides">The number of sides of the die to test.</param>
      /// <param name="numberRolled">The number to be rolled on the die.</param>
      /// <param name="targetNumber">The target number to compare against for selection.</param>
      /// <param name="die">The die to roll and compare.</param>
      /// <param name="selected">The selected die.</param>
      [Scenario]
      [Example(10, 6, 7)]
      [Example(10, 5, 7)]
      [Example(10, 1, 7)]
      [Example(4, 1, 2)]
      [Example(4, 3, 4)]
      public void TargetNumberSelectorCorrectlyFailsToSelectDie(
         int numberOfSides,
         int numberRolled,
         int targetNumber,
         IDie die,
         IDie selected)
      {
         "Rolling a new die"
            .x(() => (die = NumericDiceGenerator.NewNumericTestDie(numberOfSides, new List<int> { numberRolled })).Roll());

         "And evaluating it against a target number selector"
            .x(() => selected = new TargetNumberSelector(targetNumber).EvaluateDieForSelection(die));

         "Results in the die being not selected"
            .x(() => selected.Should().BeNull());
      }

      /// <summary>
      ///    Tests to a <see cref="TargetNumberSelector" /> to insure that dice are selected when appropriate.
      /// </summary>
      /// <param name="numberOfSides">The number of sides of the die to test.</param>
      /// <param name="numberRolled">The number to be rolled on the die.</param>
      /// <param name="targetNumber">The target number to compare against for selection.</param>
      /// <param name="die">The die to roll and compare.</param>
      /// <param name="selected">The selected die.</param>
      [Scenario]
      [Example(10, 7, 7)]
      [Example(10, 8, 7)]
      [Example(10, 9, 7)]
      [Example(10, 10, 7)]
      [Example(4, 1, 1)]
      [Example(4, 4, 4)]
      public void TargetNumberSelectorCorrectlySelectsDie(
         int numberOfSides,
         int numberRolled,
         int targetNumber,
         IDie die,
         IDie selected)
      {
         "Rolling a new die"
            .x(() => (die = NumericDiceGenerator.NewNumericTestDie(numberOfSides, new List<int> { numberRolled })).Roll());

         "And evaluating it against a target number selector"
            .x(() => selected = new TargetNumberSelector(targetNumber).EvaluateDieForSelection(die));

         "Results in the die being selected"
            .x(() => selected.Should().NotBeNull().And.BeSameAs(die));
      }
   }
}