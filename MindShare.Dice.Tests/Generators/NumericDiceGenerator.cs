﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="NumericDiceGenerator.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using System.Collections.Generic;

using MindShare.Dice.Interfaces;
using MindShare.Dice.Tests.Mocks;

using Org.Edgerunner.FluentGuard;

namespace MindShare.Dice.Tests.Generators
{
   public static class NumericDiceGenerator
   {
      /// <summary>
      ///    Creates a list of test dice.
      /// </summary>
      /// <param name="numberOfSides">The number of sides that each die should have.</param>
      /// <param name="numberOfDice">The number of dice to create.</param>
      /// <param name="numbersToRoll">The number that each of the die should roll.</param>
      /// <returns>A new <see cref="List{IDie}" /> of test dice.</returns>
      /// <remarks>Each number from the <paramref name="numbersToRoll" /> is assigned to a single die in sequence.</remarks>
      public static List<IDie> CreateDice(int numberOfSides, int numberOfDice, params int[] numbersToRoll)
      {
         // Verify that we have enough test numbers to support our dice
         Validate.That(nameof(numbersToRoll.Length), numbersToRoll.Length).IsEqualTo(numberOfDice).OtherwiseThrowException();

         var result = new List<IDie>(numberOfDice);
         for (int i = 1; i < numberOfDice; i++)
            result.Add(NewNumericTestDie(numberOfSides, new List<int> { numbersToRoll[i - 1] }));
         return result;
      }

      public static IDie NewD10(IList<int> numbersToRoll)
      {
         return NewNumericTestDie(10, numbersToRoll);
      }

      public static IDie NewD100(IList<int> numbersToRoll)
      {
         return NewNumericTestDie(100, numbersToRoll);
      }

      public static IDie NewD12(IList<int> numbersToRoll)
      {
         return NewNumericTestDie(12, numbersToRoll);
      }

      public static IDie NewD20(IList<int> numbersToRoll)
      {
         return NewNumericTestDie(20, numbersToRoll);
      }

      public static IDie NewD4(IList<int> numbersToRoll)
      {
         return NewNumericTestDie(4, numbersToRoll);
      }

      public static IDie NewD6(IList<int> numbersToRoll)
      {
         return NewNumericTestDie(6, numbersToRoll);
      }

      public static IDie NewD8(IList<int> numbersToRoll)
      {
         return NewNumericTestDie(8, numbersToRoll);
      }

      public static IDie NewNumericTestDie(int numberOfSides, IList<int> numbersToRoll)
      {
         var sides = new List<int>(numberOfSides);
         for (int i = 1; i <= numberOfSides; i++)
            sides.Add(i);
         return new NumericTestDie(sides, numbersToRoll);
      }
   }
}