﻿#region Apache License 2.0

// <copyright company="Edgerunner.org" file="NumericPoolGenerator.cs">
// Copyright (c)  2017
// </copyright>
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#endregion

using MindShare.Dice.Interfaces;

namespace MindShare.Dice.Tests.Generators
{
   public static class NumericPoolGenerator
   {
      public static IDicePool NewPool(int numberOfSides, int numberOfDice, params int[] numbersToRoll)
      {
         return new DicePool(NumericDiceGenerator.CreateDice(numberOfSides, numberOfDice, numbersToRoll));
      }
   }
}